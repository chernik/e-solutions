﻿var R = (function(){
	var obj = {};
	
	var op = { // C => C
		"--": function(a){ // 0 - a // "-000" => "000"; "000" => "-000"
			if(a[0] == "-"){
				a = a.split("");
				a.shift();
				return a.join("");
			}
			return "-" + a;
		},
		"++": function(a){ // "-000" => "0"
			if(a[0] == "-")
				a = this["--"](this["++"](this["--"](a)));
			else{
				a = a.split("");
				while(a.length >= 2 && a[0] == "0")
					a.shift();
				a = a.join("");
			}
			if(a == "-0")
				a = "0";
			return a;
		},
		"+": function(a, b){ // a + b
			if(a[0] == "-")
				return this["-"](b, this["--"](a));
			if(b[0] == "-")
				return this["-"](a, this["--"](b));
			while(b.length > a.length)
				a = "0" + a;
			while(b.length < a.length)
				b = "0" + b;
			var c = "";
			var buf = 0;
			var i;
			for(i = a.length - 1; i >= 0; i--){
				buf = buf + (+a[i]) + (+b[i]);
				c = "" + (buf % 10) + c;
				buf = Math.floor(buf / 10);
			}
			if(buf > 0)
				c = "" + buf + c;
			return this["++"](c);
		},
		"-": function(a, b){ // a - b
			if(a[0] == "-")
				return this["--"](this["+"](this["--"](a), b));
			if(b[0] == "-")
				return this["+"](a, this["--"](b));
			while(b.length > a.length)
				a = "0" + a;
			while(b.length < a.length)
				b = "0" + b;
			var c = "";
			var buf = 0;
			var i;
			for(i = a.length - 1; i >= 0; i--){
				var d = (+a[i]) - (+b[i]) - buf;
				buf = 0;
				while(d < 0){
					buf++
					d += 10;
				}
				c = "" + d + c;
			}
			if(buf > 0)
				return this["--"](this["-"](b, a));
			return this["++"](c);
		},
		"*": function(a, b){ // a * b
			if(a[0] == "-")
				return this["--"](this["*"](this["--"](a), b));
			if(b[0] == "-")
				return this["--"](this["*"](a, this["--"](b)));
			if(a.length < b.length)
				return this["*"](b, a);
			if(b.length == 1){
				var c = "";
				var i;
				var buf = 0;
				for(i = a.length - 1; i >= 0; i--){
					buf = (+a[i]) * (+b) + buf;
					c = "" + (buf % 10) + c;
					buf = Math.floor(buf / 10);
				}
				if(buf > 0)
					c = "" + buf + c;
				return this["++"](c);
			}
			var zr = "";
			var i;
			var sum = "0";
			for(i = b.length - 1; i >= 0; i--){
				sum = this["+"](sum, this["*"](a, b[i]) + zr);
				zr += "0";
			}
			return this["++"](sum);
		},
		"/": function(a, b){ // a / b
			if(a[0] == "-")
				return [this["--"](this["/"](this["--"](a), b)[0]), "0"];
			if(b[0] == "-")
				return [this["--"](this["/"](a, this["--"](b))[0]), "0"];
			if(b == "0")
				return ["0", "0"];
			var num = a.split("");
			var buf = "";
			var rez = "";
			var k;
			while(true){
				buf = this["++"](buf + num.shift());
				k = 0;
				var buf2 = buf;
				do{
					buf = buf2;
					buf2 = this["-"](buf, b);
					k++;
				}while(buf2[0] != "-");
				k--;
				rez += "" + k;
				if(num.length == 0)
					break;
			}
			return [this["++"](rez), this["++"](buf)];
		},
		["NOD"]: function(a, b){
			if(a[0] == "-")
				return this["NOD"](this["--"](a), b);
			if(b[0] == "-")
				return this["NOD"](a, this["--"](b));
			if(this["-"](a, b)[0] == "-")
				return this["NOD"](b, a);
			while(b != 0){
				var c = this["/"](a, b)[1];
				a = b;
				b = c;
			}
			return a;
		}
	};
	
	function normalize(a){ // Q => Q nesokr
		if(a != a)
			return NaN;
		var b = op["NOD"](a.x, a.y);
		var c = {};
		c.x = op["/"](a.x, b)[0];
		c.y = op["/"](a.y, b)[0];
		if(c.y[0] == "-"){
			c.x = op["--"](c.x);
			c.y = op["--"](c.y);
		}
		return c;
	}
	
	function num2str(a){ // R => Q
		if(a != a)
			return NaN;
		if(typeof(a) == typeof(""))
			return {x: a, y: "1"};
		if(a == Infinity || a == -Infinity)
			return NaN;
		var sgna = 1;
		if(a < 0){
			sgna = -1;
			a = -a;
		}
		var b = "";
		while(Math.floor(a) != a){
			a *= 10;
			b += "0";
		}
		var delim = (1000).toLocaleString()[1];
		a = a.toLocaleString().split(delim).join("");
		var c = {};
		c.x = a;
		c.y = "1" + b;
		if(sgna < 0)
			c.x = op["--"](c.x);
		return normalize(c);
	}
	
	obj.create = function(a, b){
		if(b == undefined || b == 0){
			if(a == undefined)
				return this.create(1, 1);
			if(typeof(a) == typeof({}))
				return {x: a.x, y: a.y};
			return num2str(a);
		}
		return this.del(num2str(a), num2str(b));
	}
	
	obj.plus = function(a, b){
		if(a != a || b != b)
			return NaN;
		var c = op["NOD"](a.y, b.y);
		var d1 = op["/"](a.y, c)[0];
		var d2 = op["/"](b.y, c)[0];
		var d = {};
		d.x = op["+"](
			op["*"](a.x, d2),
			op["*"](b.x, d1));
		d.y = op["*"](a.y, d2);
		return normalize(d);
	}
	
	obj.minus = function(a, b){
		if(a != a || b != b)
			return NaN;
		var c = this.create(b);
		c.x = op["--"](c.x);
		return obj.plus(a, c);
	}
	
	obj.mult = function(a, b){
		if(a != a || b != b)
			return NaN;
		var c = {};
		c.x = op["*"](a.x, b.x);
		c.y = op["*"](a.y, b.y);
		return normalize(c);
	}
	
	obj.del = function(a, b){
		if(a != a || b != b)
			return NaN;
		var c = {};
		c.x = op["*"](a.x, b.y);
		c.y = op["*"](a.y, b.x);
		return normalize(c);
	}
	
	obj.sum = function(){
		var a = arguments;
		var i = 0;
		var s = this.create(0);
		while(arguments[i] != undefined){
			s = this.plus(s, arguments[i]);
			i++;
		}
		return s;
	}
	
	obj.pow = function(a, b){
		if(a != a || b != b)
			return NaN;
		if(b.y != "1") // NOO
			return NaN;
		if(b.x[0] == "-"){
			var c = this.create(b);
			c.x = op["--"](c.x);
			a = this.del(this.create(), a);
			return this.pow(a, c);
		}
		if(b.x == "1")
			return this.create(a);
		return this.mult(a, this.pow(a, this.minus(b, this.create())));
	}
	
	obj.foo = function(){
		return op;
	}
	
	return obj;
})();